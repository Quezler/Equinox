package net.queztech.equinox;

import java.io.File;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Equinox extends JavaPlugin {
	private itemstack itemstack;
	private sheep sheep;
	private Usage usage;
    // Fired when plugin is first enabled
    @Override
    public void onEnable() {
    	this.createConfig();
    	
    	this.itemstack = new itemstack(this);
    	getServer().getPluginManager().registerEvents(itemstack, this);
    	
    	this.sheep = new sheep(this);
    	getServer().getPluginManager().registerEvents(sheep, this);
    	
    	this.usage = new Usage(this);
    	getServer().getPluginManager().registerEvents(usage, this);
    }
    // Fired when plugin is disabled
    @Override
    public void onDisable() {
    	this.itemstack.down();
    }
    
	public void drop(Location loc, ItemStack item) {
		loc.getWorld().dropItemNaturally(loc.add( new Location(loc.getWorld(), 0.5, 0.5, 0.5) ), item);
	}
	
    private void createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
            } else {
                getLogger().info("Config.yml found, loading!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
