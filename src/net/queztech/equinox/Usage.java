package net.queztech.equinox;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class Usage implements Listener {

	private Equinox plugin;
	public Usage(Equinox equinox) {
		this.plugin = equinox;
	}

	@EventHandler
	public void OnPlayerLogin(PlayerLoginEvent event) {
		if (plugin.getConfig().getBoolean("metrics") == false) {
			return;
		}
		String hostname = event.getHostname();
		String name = plugin.getName();
		String version = "v1.2.1";
		
		String urlString = "http://ci.equinox.queztech.net/?hostname="+hostname+"&name="+name+"&version=" + version;
		try {
			URL url = new URL(urlString);
			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
}
