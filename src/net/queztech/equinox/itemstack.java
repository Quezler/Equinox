package net.queztech.equinox;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.craftbukkit.v1_9_R1.entity.CraftItem;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.inventory.ItemStack;

public class itemstack implements Listener {
	
	private Equinox plugin;
	List<ItemStack> cache = new ArrayList<ItemStack>();

	public itemstack(Equinox equinox) {
		this.plugin = equinox;
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void OnItemDeath(EntityDamageEvent event) {
		Entity le = event.getEntity();
		
		if (le.getType().equals(EntityType.DROPPED_ITEM)) {
			CraftItem item = (CraftItem) event.getEntity();
			ItemStack is = item.getItemStack();
			this.cache.add(is);
			event.getEntity().remove();
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void OnItemDespawn(ItemDespawnEvent event) {
		this.cache.add(event.getEntity().getItemStack());
	}
	
	private ItemStack pull() {
		if (this.cache.size() == 0) {
			return null;
		}
		Random randomizer = new Random();
		Integer key = randomizer.nextInt(this.cache.size());
		ItemStack random = this.cache.get(key);
						   this.cache.remove(key);
						   
		return random;
	}
	
	@EventHandler
	public void BlockBreakEvent(BlockBreakEvent event) {
		ItemStack item = this.pull();
		if (new Random().nextInt(10)==0 && item != null) {
			plugin.drop(event.getBlock().getLocation(), item);
		}
	}
	public void down() {
		if (this.cache.size() == 0) {
			return;
		}
		
		List<Player> players = (List<Player>) plugin.getServer().getOnlinePlayers();
		Random randomizer = new Random();
		for (ItemStack item : this.cache) {
			Player player = players.get(randomizer.nextInt(players.size()));
			plugin.drop(player.getLocation(), item);
		}
	}
	
}
