package net.queztech.equinox;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.inventory.ItemStack;

public class sheep implements Listener {
	
	private Equinox plugin;

	public sheep(Equinox equinox) {
		this.plugin = equinox;
	}

	@EventHandler
	public void OnSheepShear(PlayerShearEntityEvent event) {
		if (event.getEntity().getType().equals(EntityType.SHEEP)) {
			Player p = event.getPlayer();
			
			for (ItemStack armor : p.getInventory().getArmorContents()) {
				if (armor != null) {
					plugin.drop(p.getLocation(), armor);
				}
			}
			ItemStack[] is = new ItemStack[0];
			p.getInventory().setArmorContents(is);
			p.updateInventory();
		}
	}
}
